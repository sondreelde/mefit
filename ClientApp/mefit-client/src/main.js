import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Home from './components/Home.vue'
import ViewProfile from './components/ViewProfile.vue'
import ViewProfiles from './components/ViewProfiles.vue'
import CreateProfile from './components/CreateProfile.vue'
import ViewExercises from './components/ViewExercises.vue'
import ViewExercise from './components/ViewExercise.vue'
import CreateExercise from './components/CreateExercise.vue'
import UpdateExercise from './components/UpdateExercise.vue'
import CreateGoal from './components/CreateGoal.vue'

Vue.use(BootstrapVue)
Vue.use(VueRouter)

Vue.config.productionTip = false

const routes = [
  { path: '/', name: 'home', component: Home },
  { path: '/profile', name: 'ViewProfile', component: ViewProfile },
  { path: '/profiles', name: 'ViewProfiles', component: ViewProfiles },
  { path: '/profile/create', name: 'CreateProfile', component: CreateProfile},
  { path: '/exercises', name: 'ViewExercises', component: ViewExercises},
  { path: '/exercise', name: 'ViewExercise', component: ViewExercise},
  { path: '/exercise/create', name: 'CreateExercise', component: CreateExercise},
  { path: '/exercise/update', name: 'UpdateExercise', component: UpdateExercise},
  { path: '/goal', name: 'CreateGoal', component: CreateGoal}
]

const router = new VueRouter({
  routes: routes,
  mode: 'history'
})

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
