﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFit.Migrations
{
    public partial class seedquestion11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "PostalCodes",
                columns: new[] { "Code", "CountryId" },
                values: new object[] { 5009, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PostalCodes",
                keyColumn: "Code",
                keyValue: 5009);
        }
    }
}
