﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFit.Migrations
{
    public partial class seedalot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "Image", "Name", "TargetMuscleGroup", "VideoLink" },
                values: new object[,]
                {
                    { 1, "Lie on the floor and crunch", "Google it", "Crunches", "Abs", "Youtube it" },
                    { 2, "Lie on your stomach and push witht the arms.", "Google it", "Push ups", "Pecks", "Youtube it" },
                    { 3, "Stand and squat down.", "Google it", "Squats", "Thighs", "Youtube it" },
                    { 4, "Do bench press", "Google it", "Bench press", "Pecks", "Youtube it" },
                    { 5, "Stand and squat down with weight.", "Google it", "Squats w/ barbell", "Thighs", "Youtube it" },
                    { 6, "Stand with weight in your arms, put it down and lift it up.", "Google it", "Deadlift", "Core", "Youtube it" },
                    { 7, "Run", "Google it", "Treadmill", "Cardio", "Youtube it" }
                });

            migrationBuilder.InsertData(
                table: "Programs",
                columns: new[] { "Id", "Category", "Name" },
                values: new object[] { 1, "Strength", "Super Program" });

            migrationBuilder.InsertData(
                table: "Workouts",
                columns: new[] { "Id", "Name", "Type" },
                values: new object[,]
                {
                    { 1, "Monday", "Legs" },
                    { 2, "Wednesday", "Push" },
                    { 3, "Friday", "Pull" }
                });

            migrationBuilder.InsertData(
                table: "Goal",
                columns: new[] { "Id", "Description", "ExerciseId", "IsReached" },
                values: new object[,]
                {
                    { 1, "Double the load", 1, false },
                    { 2, "Half the time", 2, false },
                    { 3, "On one foot", 3, false }
                });

            migrationBuilder.InsertData(
                table: "ProfilePrograms",
                columns: new[] { "ProfileId", "ProgramId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "ProgramWorkouts",
                columns: new[] { "ProgramId", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 3 },
                    { 1, 2 },
                    { 1, 1 }
                });

            migrationBuilder.InsertData(
                table: "Sets",
                columns: new[] { "Id", "ExerciseId", "Repititions" },
                values: new object[,]
                {
                    { 3, 3, "3:30" },
                    { 2, 2, "3:20" },
                    { 1, 1, "3:30" },
                    { 4, 4, "3:5" }
                });

            migrationBuilder.InsertData(
                table: "WorkoutExercises",
                columns: new[] { "WorkoutId", "ExerciseId" },
                values: new object[,]
                {
                    { 3, 6 },
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 2, 4 },
                    { 2, 5 },
                    { 3, 7 }
                });

            migrationBuilder.InsertData(
                table: "ProfileGoals",
                columns: new[] { "ProfileId", "GoalId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "ProfileGoals",
                columns: new[] { "ProfileId", "GoalId" },
                values: new object[] { 1, 2 });

            migrationBuilder.InsertData(
                table: "ProfileGoals",
                columns: new[] { "ProfileId", "GoalId" },
                values: new object[] { 1, 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProfileGoals",
                keyColumns: new[] { "ProfileId", "GoalId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "ProfileGoals",
                keyColumns: new[] { "ProfileId", "GoalId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "ProfileGoals",
                keyColumns: new[] { "ProfileId", "GoalId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "ProfilePrograms",
                keyColumns: new[] { "ProfileId", "ProgramId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "ProgramWorkouts",
                keyColumns: new[] { "ProgramId", "WorkoutId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "ProgramWorkouts",
                keyColumns: new[] { "ProgramId", "WorkoutId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "ProgramWorkouts",
                keyColumns: new[] { "ProgramId", "WorkoutId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "WorkoutExercises",
                keyColumns: new[] { "WorkoutId", "ExerciseId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "WorkoutExercises",
                keyColumns: new[] { "WorkoutId", "ExerciseId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "WorkoutExercises",
                keyColumns: new[] { "WorkoutId", "ExerciseId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "WorkoutExercises",
                keyColumns: new[] { "WorkoutId", "ExerciseId" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "WorkoutExercises",
                keyColumns: new[] { "WorkoutId", "ExerciseId" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "WorkoutExercises",
                keyColumns: new[] { "WorkoutId", "ExerciseId" },
                keyValues: new object[] { 3, 6 });

            migrationBuilder.DeleteData(
                table: "WorkoutExercises",
                keyColumns: new[] { "WorkoutId", "ExerciseId" },
                keyValues: new object[] { 3, 7 });

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Goal",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Goal",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Goal",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
