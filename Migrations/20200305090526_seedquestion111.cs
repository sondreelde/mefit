﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFit.Migrations
{
    public partial class seedquestion111 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "AddressLine1", "AddressLine2", "PostalCodeId" },
                values: new object[] { 1, "Bjørniss 32", null, 5009 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
