﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFit.Migrations
{
    public partial class seedquestion1111 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AddressId", "FirstName", "IsAdmin", "IsContributor", "LastName", "Password" },
                values: new object[] { 1, 1, "Sondre", false, false, "Eldis", "Hunter123" });

            migrationBuilder.InsertData(
                table: "Profiles",
                columns: new[] { "Id", "Height", "UserId", "Weight" },
                values: new object[] { 1, 190.0, 1, 77.0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
