﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MeFit.Models;

namespace MeFit.Data
{
    public class MeFitContext : DbContext
    {
        public MeFitContext (DbContextOptions<MeFitContext> options)
            : base(options)
        {
        }

        public DbSet<MeFit.Models.Profile> Profiles { get; set; }
        public DbSet<MeFit.Models.User> Users { get; set; }
        public DbSet<MeFit.Models.Address> Addresses { get; set; }
        public DbSet<MeFit.Models.PostalCode> PostalCodes { get; set; }
        public DbSet<MeFit.Models.Country> Countries { get; set; }
        public DbSet<MeFit.Models.Exercise> Exercises { get; set; }
        public DbSet<MeFit.Models.Set> Sets { get; set; }
        public DbSet<MeFit.Models.ProfileGoal> ProfileGoals { get; set; }
        public DbSet<MeFit.Models.Program> Programs { get; set; }
        public DbSet<MeFit.Models.ProfileProgram> ProfilePrograms { get; set; }
        public DbSet<MeFit.Models.Workout> Workouts { get; set; }
        public DbSet<MeFit.Models.ProgramWorkout> ProgramWorkouts { get; set; }
        public DbSet<MeFit.Models.WorkoutExercise> WorkoutExercises { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder
        .UseLazyLoadingProxies();


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProfileGoal>().HasKey(pq => new { pq.ProfileId, pq.GoalId });
            modelBuilder.Entity<ProfileProgram>().HasKey(pq => new { pq.ProfileId, pq.ProgramId });
            modelBuilder.Entity<ProgramWorkout>().HasKey(pq => new { pq.ProgramId, pq.WorkoutId });
            modelBuilder.Entity<WorkoutExercise>().HasKey(pq => new { pq.WorkoutId, pq.ExerciseId });

            //Seed data
            modelBuilder.Entity<Country>().HasData(
                new Country() { Id = 1, Name = "Norway"},
                new Country() { Id = 2, Name = "Sweeden"},
                new Country() { Id = 3, Name = "Uganda"},
                new Country() { Id = 4, Name = "Wales"}
                );

            modelBuilder.Entity<PostalCode>().HasData(
                new PostalCode() { CountryId = 1, Code = 5009}
                );

            modelBuilder.Entity<Address>().HasData(
                new Address() { AddressLine1="Bjørniss 32", PostalCodeId = 5009, Id = 1}
                );

            modelBuilder.Entity<User>().HasData(
                new User() { Id = 1, AddressId = 1, FirstName = "Sondre", IsAdmin = false, IsContributor = false, LastName="Eldis", Password = "Hunter123"}
                );

            modelBuilder.Entity<Profile>().HasData(
                new Profile() { Id = 1, Height = 190, UserId = 1, Weight = 77}
                );

            modelBuilder.Entity<MeFit.Models.Program>().HasData(
                new MeFit.Models.Program() { Id = 1, Name = "Super Program", Category="Strength" }
                );

            modelBuilder.Entity<ProfileProgram>().HasData(
                new ProfileProgram() { ProfileId = 1, ProgramId = 1}
                );

            modelBuilder.Entity<Workout>().HasData(
                new Workout() { Id = 1, Name = "Monday", Type="Legs"},
                new Workout() { Id = 2, Name = "Wednesday", Type = "Push" },
                new Workout() { Id = 3, Name = "Friday", Type = "Pull"}
                );

            modelBuilder.Entity<ProgramWorkout>().HasData(
                new ProgramWorkout() { ProgramId = 1, WorkoutId = 1 },
                new ProgramWorkout() { ProgramId = 1, WorkoutId = 2 },
                new ProgramWorkout() { ProgramId = 1, WorkoutId = 3 }
                );

            modelBuilder.Entity<Exercise>().HasData(
                new Exercise() { Id = 1, Name = "Crunches", Description="Lie on the floor and crunch", Image="Google it", TargetMuscleGroup = "Abs", VideoLink="Youtube it"},
                new Exercise() { Id = 2, Name = "Push ups", TargetMuscleGroup = "Pecks", Description="Lie on your stomach and push witht the arms.", Image = "Google it", VideoLink = "Youtube it" },
                new Exercise() { Id = 3, Name = "Squats", TargetMuscleGroup = "Thighs", Description = "Stand and squat down.", Image = "Google it", VideoLink = "Youtube it" },
                new Exercise() { Id = 4, Name = "Bench press", TargetMuscleGroup = "Pecks", Description = "Do bench press", Image = "Google it", VideoLink = "Youtube it" },
                new Exercise() { Id = 5, Name = "Squats w/ barbell", TargetMuscleGroup = "Thighs", Description = "Stand and squat down with weight.", Image = "Google it", VideoLink = "Youtube it" },
                new Exercise() { Id = 6, Name = "Deadlift", TargetMuscleGroup = "Core", Description = "Stand with weight in your arms, put it down and lift it up.", Image = "Google it", VideoLink = "Youtube it" },
                new Exercise() { Id = 7, Name = "Treadmill", TargetMuscleGroup = "Cardio", Description = "Run", Image = "Google it", VideoLink = "Youtube it" }
                );

            modelBuilder.Entity<Set>().HasData(
                new Set() { Id = 1, ExerciseId = 1, Repititions = "3:30" },
                new Set() { Id = 2, ExerciseId = 2, Repititions = "3:20" },
                new Set() { Id = 3, ExerciseId = 3, Repititions = "3:30" },
                new Set() { Id = 4, ExerciseId = 4, Repititions = "3:5" }
                );
            modelBuilder.Entity<WorkoutExercise>().HasData(
                new WorkoutExercise() { ExerciseId = 1, WorkoutId = 1 },
                new WorkoutExercise() { ExerciseId = 2, WorkoutId = 1 },
                new WorkoutExercise() { ExerciseId = 3, WorkoutId = 1 },
                new WorkoutExercise() { ExerciseId = 4, WorkoutId = 2 },
                new WorkoutExercise() { ExerciseId = 5, WorkoutId = 2 },
                new WorkoutExercise() { ExerciseId = 6, WorkoutId = 3 },
                new WorkoutExercise() { ExerciseId = 7, WorkoutId = 3 }
                );

            modelBuilder.Entity<Goal>().HasData(
                new Goal() { Id = 1, IsReached = false, Description = "Double the load", ExerciseId = 1 },
                new Goal() { Id = 2, IsReached = false, Description = "Half the time", ExerciseId = 2 },
                new Goal() { Id = 3, IsReached = false, Description = "On one foot", ExerciseId = 3 }
                );

            modelBuilder.Entity<ProfileGoal>().HasData(
                new ProfileGoal() { ProfileId = 1, GoalId = 1 },
                new ProfileGoal() { ProfileId = 1, GoalId = 2 },
                new ProfileGoal() { ProfileId = 1, GoalId = 3 }
                );


        }


        public DbSet<MeFit.Models.Goal> Goal { get; set; }

    }
}
