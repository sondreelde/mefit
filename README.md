# MeFit

The frontend and backend are not connected, but run separatley.

Start the API in a C# compile (ISS Express) and then start the frontend app.
The fronendapp is inside the ClientApp directory. Check the readme insed on how 
to run that.

# MeFit API

Compile in a C# compiler.

## Description

Provides a crud endpoints for a profile in a fitness app.

### GET

List of all profiles : [...]/api/profiles

One profile : [...]/api/profiles/3

### PUT 

[...]/api/profiles/3

### POST

[...]/api/profiles

### DELETE

[...]/api/Profiles/3

## Content

### ProfilesController.cs

Controls the endpoints.

### MeFitContext.cs

The dbcontext for setting up daatbase with entity framework.

### Models

Classes representing the tables in the database.


## Contributing

Sondre Elde (https://gitlab.com/sondreelde)
