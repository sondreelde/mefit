﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit.Models
{
    public class ProgramWorkout
    {
        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }
        public int WorkoutId { get; set; }
        public virtual Workout Workout { get; set; }
    }
}
