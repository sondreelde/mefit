﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit.Models
{
    public class ProfileProgram
    {
        public int ProfileId { get; set; }
        public virtual Profile Profile { get; set; }
        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }
    }
}
