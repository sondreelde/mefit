﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit.Models
{
    public class Workout
    {
        public int Id { get; set;  }
        public string Name { get; set; }
        public string Type { get; set; }
        public virtual ICollection<ProgramWorkout> ProgramWorkouts { get; set; }
        public virtual ICollection<WorkoutExercise> WorkoutExercises { get; set; }
    }
}
