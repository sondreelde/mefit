﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MeFit.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public virtual ICollection<ProfileGoal> ProfileGoals { get; set; }
        public virtual ICollection<ProfileProgram> ProfilePrograms { get; set; }
    }
}
