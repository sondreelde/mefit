﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MeFit.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int PostalCodeId { get; set; }
        public virtual PostalCode PostalCode { get; set; }
        public virtual ICollection<User> User { get; set; }

    }
}
