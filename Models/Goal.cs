﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit.Models
{
    public class Goal
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsReached { get; set; }
        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }
        public virtual ICollection<ProfileGoal> ProfileGoals { get; set; }
    }
}
