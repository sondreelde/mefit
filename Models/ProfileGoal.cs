﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit.Models
{
    public class ProfileGoal
    {
        [Key]
        public int ProfileId { get; set; }
        public virtual Profile Profile { get; set; }
        [Key]
        public int GoalId { get; set; }
        public virtual Goal Goal { get; set; }
    }
}
